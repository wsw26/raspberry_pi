RASPBERRY PI 1----------------------------
wlan1: 192.168.0.200 (static IP)
eth1: 169.254.42.78

RASPBERRY PI 2----------------------------
wlan2: 192.168.0.210 (static IP)
eth2: 169.254.89.114

RASPBERRY PI 3----------------------------
wlan: 192.168.0.220 (static IP)
eth3: 169.254.206.13

RASPBERRY PI 4----------------------------
wlan: 192.168.0.230 (static IP)
eth4: 169.254.36.235


pi backup
eth: 169.254.200.193



to configure ssh-keygen: https://www.thegeekstuff.com/2008/11/3-steps-to-perform-ssh-login-without-password-using-ssh-keygen-ssh-copy-id

opencv pi installation : https://www.youtube.com/watch?v=Ha2CcszCNGk

to stream from webcam______________________________________________

	install xming and xming fonts
	enable x11 port forwarding @ pi through : sudo nano /etc/ssh/sshd_config
	look for xforwarding yes
	open putty, load configuration, enable x11 port forwarding



to replace defective pi with pi backup 1:

1. run shutdown/shutdown.eth
2. unplug power
3. GENTLY take out the SD card from the defective pi and slot it into the backup pi
4. go to these files on the PC and change these codes:

	i. snap_image/snap_eth: comment the ssh command of the defective pi and uncomment the backup pi ssh command.
	   Do the same for teh scp command. Take note to also change the pi(x).png file name. x refers to the defective pi number.
	ii. open shutdown/shutdown_eth. comment the ssh command of the defective pi and uncomment the backup pi ssh command.
	iii. open reboot/reboot. comment the ssh command of the defective pi and uncomment the backup pi ssh command.
	iv. open preview_picam/preview_eth. comment the start command of the defective pi and uncomment the backup pi start vlc command.
	v.  open preview_picam/preview_eth.py change the pi ip address of the defective pi to 'pi@169.254.200.193'


