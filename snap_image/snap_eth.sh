#!/bin/sh

echo "taking image from camera 1"
ssh pi@10.100.222.105 "python3 /home/pi/Desktop/take_image.py" &

echo "taking image from camera 2"
ssh pi@10.100.222.106 "python3 /home/pi/Desktop/take_image.py" &

echo "taking image from camera 3"
ssh pi@10.100.222.107 "python3 /home/pi/Desktop/take_image.py" &

echo "taking image from camera 4"
ssh pi@10.100.222.108 "python3 /home/pi/Desktop/take_image.py" &

#back up pi
#ssh pi@169.254.200.193 "python3 /home/pi/Desktop/take_image.py" &

wait

echo "transferring images"


scp pi@10.100.222.105:/home/pi/Desktop/pi1.png C:/Locker/gamuda_images/
scp pi@10.100.222.106:/home/pi/Desktop/pi2.png C:/Locker/gamuda_images/
scp pi@10.100.222.107:/home/pi/Desktop/pi3.png C:/Locker/gamuda_images/
scp pi@10.100.222.108:/home/pi/Desktop/pi4.png C:/Locker/gamuda_images/

#backup pi
#scp pi@169.254.200.193:/home/pi/Desktop/pi(x).png C:/Locker/gamuda_images/



python C:/Users/BlinkMp932/Desktop/raspberry_pi/update_json.py