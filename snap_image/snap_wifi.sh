#!/bin/sh

echo "taking image from camera 1"

ssh pi@192.168.0.200 "python3 /home/pi/Desktop/take_image.py" &
#ssh pi@192.168.0.200 "python /home/pi/Desktop/hik_cap.py"

echo "taking image from camera 2"
ssh pi@192.168.0.210 "python3 /home/pi/Desktop/take_image.py" &

echo "taking image from camera 3"
ssh pi@192.168.0.220 "python3 /home/pi/Desktop/take_image.py" &

echo "taking image from camera 4"
ssh pi@192.168.0.230 "python3 /home/pi/Desktop/take_image.py" &

wait


echo "transferring images"

#scp  pi@192.168.0.200:/home/pi/Desktop/image1.jpg ~/Desktop/gamuda_images

scp pi@192.168.0.200:/home/pi/Desktop/pi1.png C:/Locker/gamuda_images/
scp pi@192.168.0.210:/home/pi/Desktop/pi2.png C:/Locker/gamuda_images/
scp pi@192.168.0.220:/home/pi/Desktop/pi3.png C:/Locker/gamuda_images/
scp pi@192.168.0.230:/home/pi/Desktop/pi4.png C:/Locker/gamuda_images/