

import subprocess


try:

    a = subprocess.Popen(['ssh', '-t', 'pi@192.168.0.200', '/home/pi/Desktop/preview_image.sh', 'a'])
    b = subprocess.Popen(['ssh', '-t', 'pi@192.168.0.210', '/home/pi/Desktop/preview_image.sh', 'b'])
    c = subprocess.Popen(['ssh', '-t', 'pi@192.168.0.220', '/home/pi/Desktop/preview_image.sh', 'c'])
    d = subprocess.Popen(['ssh', '-t', 'pi@192.168.0.230', '/home/pi/Desktop/preview_image.sh', 'd'])
    a.communicate()
    b.communicate()
    c.communicate()
    d.communicate()


except KeyboardInterrupt:
    os.kill(a.pid, signal.SIGTERM)
    os.kill(b.pid, signal.SIGTERM)
    os.kill(c.pid, signal.SIGTERM)
    os.kill(d.pid, signal.SIGTERM)
